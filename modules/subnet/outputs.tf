output "ids" {
  value = concat(aws_subnet.subnet.*.id, [""])[0]
}

output "route_table_id" {
  value = concat(aws_route_table.subnet.*.id, [""])[0]
}
