
resource "aws_nat_gateway" "nat" {
  count = length(var.subnet_ids)
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${element(var.subnet_ids, count.index)}"
}

resource "aws_eip" "nat" {
  vpc   = true
}
