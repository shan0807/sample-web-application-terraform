vpc_cidr = "10.0.0.0/16"
environment = "demo-test"
public_subnet_cidrs = ["10.0.0.0/24", "10.0.1.0/24"]
private_subnet_cidrs = ["10.0.50.0/24", "10.0.51.0/24"]
availibility_zones = ["us-west-1a", "us-west-1c"]
region = "us-west-1"
key_name = "shan-aws"
instance_type = "t2.medium"