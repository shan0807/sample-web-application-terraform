#!/bin/bash

#Install nginx server
yum update -y
yum install nginx awslogs logrotate -y
service nginx start
service awslogs start
systemctl start awslogsd
chkconfig awslogs on
systemctl enable awslogsd.service

## Log rotation

/var/log/ngnix/*.log {
weekly
missingok
rotate 4
compress
delaycompress
notifempty
create 640 root adm
sharedscripts
#postrotate
#  aws s3 sync /var/log/apache2/ s3://<Your-bucket-Name>/<Server-Name>/<Apache2-logs>/ --region <your-bucket-region>
endscript
}
