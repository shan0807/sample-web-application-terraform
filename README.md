# SAMPLE WEB APPLICATION
Created a sample web application by installing nginx on amazon Linux using terraform

## Installation

Please install terraform version 12.29 or higher and run the following, commands to export aws credentials

```bash
# Windows

set AWS_ACCESS_KEY_ID=XXXXXXXXXXXX
set AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXX

# macOS and Linux

export AWS_ACCESS_KEY_ID=XXXXXXXXXXxx
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXX


```

## Infra Creation 

```bash

terraform init
terraform plan --vars=variables.tfvars
terraform apply --vars=variables.tfvars

```

## Attributes

* All the data is encrypted at rest
* Installed SSM Session manager for taking session. Follow the steps:-
Login to the EC2 console and click on connect and select session manager to login to the instance
* CloudWatch alarms installed
* Configured ASG with Load Balancer
* Log Rotation enabled