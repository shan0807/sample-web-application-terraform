provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "mastercard_assignment_test_bucket"
    key    = "sample_web_application.tfstate"
    region = "${var.region}"
  }
}
