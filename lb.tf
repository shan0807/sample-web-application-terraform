resource "aws_lb" "lb" {
  name            = "lb"
  security_groups = [aws_security_group.lb_sg.id]
  subnets         = [module.network.public_subnet_ids[0], module.network.public_subnet_ids[1]]
  enable_deletion_protection = false
  tags = {
    Name        = "lb"
  }
}

resource "aws_lb_target_group" "tgt_grp" {
  name     = "tgt-grp"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.network.vpc_id
  health_check {
    path = "/"
  }
  target_type = "instance"
  tags = {
    Name        = "tgt-grp"
  }
}

resource "aws_lb_listener" "tg_listner_appsrv" {
  load_balancer_arn = "${aws_lb.lb.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "	arn:aws:acm:us-west-1:816461209558:certificate/91f83a7a-f960-48ac-b8a0-e7777cf91c46"

  default_action {
    target_group_arn = "${aws_lb_target_group.tgt_grp .arn}"
    type             = "forward"
  }
}
