# 1 and 2 
module "network" {
  source = "./modules/network"
  environment = "${var.environment}"
  vpc_cidr = "${var.vpc_cidr}"
  private_subnet_cidrs = "${var.private_subnet_cidrs}"
  public_subnet_cidrs = "${var.public_subnet_cidrs}"
  availibility_zones = "${var.availibility_zones}"
}

