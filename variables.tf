
variable "region" {}
variable "vpc_cidr" {}
variable "environment" {}
variable "private_subnet_cidrs" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "availibility_zones" {
  type = "list"
}

variable "key_name" {}
variable "instance_type" {}