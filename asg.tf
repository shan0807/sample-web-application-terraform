data "aws_ami" "linux_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

data "template_file" "user_data" {
  template = "${file("${path.module}/template/user-data.tpl")}"
}

resource "aws_launch_configuration" "lc" {
  name_prefix                 = "lc-"
  image_id                    = data.aws_ami.linux_ami.id
  instance_type               = var.instance_type
  key_name                    = var.key_name
  security_groups             = [aws_security_group.asg_sg.id]
  associate_public_ip_address = "false"
  user_data = data.template_file.user_data.rendered
  lifecycle { create_before_destroy = true }
  iam_instance_profile = aws_iam_instance_profile.profile.name
  ebs_block_device {
      device_name = "/dev/sdb"
      volume_type = "gp2"
      volume_size = 100
      encrypted = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                  = "asg"
  vpc_zone_identifier   = module.network.public_subnet_ids
  launch_configuration  = aws_launch_configuration.lc.name
  min_size              = 1
  max_size              = 4
  desired_capacity      = 2
  protect_from_scale_in = "true"
  target_group_arns = [ "value" ]
  lifecycle { create_before_destroy = true }
  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "asg"
        "propagate_at_launch" = true
      }
    ]
  )
}

# data "aws_iam_instance_profile" "profile" {
#   name  = "test-iam-instance"
# }

resource "aws_autoscaling_policy" "ecs-autoscaling-policy" {
  name = "ASGAverageCPUUtilization"
  adjustment_type = "ChangeInCapacity"
  policy_type = "TargetTrackingScaling"
  autoscaling_group_name = aws_autoscaling_group.asg.name
  target_tracking_configuration {  
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"  
    }
    target_value = 70.0
  }
}

resource "aws_iam_role" "role" {
  name = "ec2-role"
  description = "IAM role for ec2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
   Name = "ec2-role"
  }
}

resource "aws_iam_instance_profile" "profile" {
  name = "instance_profile"
  role = "${aws_iam_role.role.name}"
}

resource "aws_iam_role_policy_attachment" "ssm_managed_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role = "${aws_iam_role.role.name}"
}
resource "aws_iam_role_policy_attachment" "ssm_directory_service" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMDirectoryServiceAccess"
  role = "${aws_iam_role.role.name}"
}
resource "aws_iam_role_policy_attachment" "cw_access" {
  policy_arn = aws_iam_policy.cw.arn
  role = "${aws_iam_role.role.name}"
}


resource "aws_iam_policy" "cw" {
  name        = "cw-access"
  path        = "/"
  description = "cw access"
  
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
    ],
      "Resource": [
        "*"
    ]
  }
 ]
}
POLICY
}
