# 3

resource "aws_security_group" "lb_sg" {
  name        = "lb-sg"
  description = "Security Group For load balancer"
  vpc_id      = module.network.vpc_id

  tags = {
    Name = "lb-sg"
  }

  ingress {
   from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

## ASG SG ##

resource "aws_security_group" "asg_sg" {
  name        = "asg-sg"
  description = "Security Group For ASG"
  vpc_id      = module.network.vpc_id

  tags = {
    Name = "asg-sg"
  }

  ingress {
   from_port   = 80
    to_port     = 80
    protocol    = "-1"
    security_groups = [aws_security_group.lb_sg.arn]
 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

